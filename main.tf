resource "newrelic_cloud_aws_link_account" "foo" {
  arn = var.arn_id
  metric_collection_mode = "PULL"
  name = var.aws_link_account_name
}

resource "newrelic_cloud_aws_integrations" "bar" {
  linked_account_id = newrelic_cloud_aws_link_account.foo.id
  billing {
    metrics_polling_interval = 3600
  }
  cloudtrail {
    metrics_polling_interval = 300
    aws_regions              = ["us-east-1", "us-east-2"]
  }
  health {
    metrics_polling_interval = 300
  }
  trusted_advisor {
    metrics_polling_interval = 3600
  }
  vpc {
    metrics_polling_interval = 900
    aws_regions              = ["us-east-1", "us-east-2"]
    fetch_nat_gateway        = true
    fetch_vpn                = false
    tag_key                  = "tag key"
    tag_value                = "tag value"
  }
  ec2 {
    aws_regions              = ["us-east-1"]
    duplicate_ec2_tags       = true
    fetch_ip_addresses       = true
    metrics_polling_interval = 300
    tag_key                  = "tag key"
    tag_value                = "tag value"
  }
}